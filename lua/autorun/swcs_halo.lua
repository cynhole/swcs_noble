game.AddParticles("particles/csgo/halo_weapon_fx.pcf")
--game.AddParticles("particles/weapons/covenant_carbine.pcf")
PrecacheParticleSystem("carbine_tracer")
PrecacheParticleSystem("srs99_tracer")
PrecacheParticleSystem("energy_sword_lunge")

if CLIENT then
	local mat = Material("swcs/icon_noble.png")
	if not mat:IsError() then
		list.Set("ContentCategoryIcons", "NOBLE Strike", "swcs/icon_noble.png")
	end

	matproxy.Add({
		name = "NobleStrike_AMMO",
		init = function( self, mat, values )
			self.ResultTo = values.resultvar
			self.Prefix = values.prefixstring
		end,
		bind = function( self, mat, ent )
			local Place = self.ResultTo
			local texture

			local ply = LocalPlayer() -- or spectating player
			if not ply:IsValid() then return end

			local wep = ply:GetActiveWeapon()

			if wep:IsValid() then
				local strAmmo = string.sub(string.reverse(wep:Clip1()), Place, Place)
				local digits = string.format( tonumber(strAmmo) or 0 )

				texture = string.format("%s%s", self.Prefix, digits)
			end

			if texture then
				mat:SetTexture( "$basetexture", texture )
			end
		end
	})
end
SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "M45 Shotgun"
SWEP.Spawnable = true
SWEP.HoldType = "shotgun"
SWEP.WorldModel = Model"models/weapons/csgo/w_shot_m45.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_shot_m45.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/m45.png", "smooth")
	swcs.AddKillicon("weapon_swcs_m45", "hud/swcs/select/m45.png")
end

SWEP.Slot = 3

sound.Add({
	name = "Weapon_M45_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 0.8,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/m45/m45-1.wav", Sound")weapons/csgo/m45/m45-2.wav", Sound")weapons/csgo/m45/m45-3.wav"}
})
sound.Add({
	name = "Weapon_M45_CSGO.Draw",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.65,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/srs99/carbine_ready1.wav"
})
sound.Add({
	name = "Weapon_M45_CSGO.PumpBack",
	channel = CHAN_ITEM,
	volume = 0.6,
	pitch = {90, 110},
	level = 65,
	sound = {Sound")weapons/csgo/m45/m45_pumpback1.wav", Sound")weapons/csgo/m45/m45_pumpback2.wav"}
})
sound.Add({
	name = "Weapon_M45_CSGO.PumpForward",
	channel = CHAN_ITEM,
	volume = 0.6,
	pitch = {90, 110},
	level = 65,
	sound = {Sound")weapons/csgo/m45/m45_pumpforward1.wav", Sound")weapons/csgo/m45/m45_pumpforward2.wav"}
})
sound.Add({
	name = "Weapon_M45_CSGO.InsertShell",
	channel = CHAN_ITEM,
	volume = {0.5, 0.6},
	pitch = {90, 110},
	level = 65,
	sound = {
		Sound")weapons/csgo/m45/m45_insert1.wav", Sound")weapons/csgo/m45/m45_insert2.wav", Sound")weapons/csgo/m45/m45_insert3.wav",
		Sound")weapons/csgo/m45/m45_insert4.wav", Sound")weapons/csgo/m45/m45_insert5.wav", Sound")weapons/csgo/m45/m45_insert6.wav",
		Sound")weapons/csgo/m45/m45_insert7.wav", Sound")weapons/csgo/m45/m45_insert8.wav"
	}
})
sound.Add({
	name = "Weapon_M45_CSGO.WeaponMove",
	channel = CHAN_ITEM,
	volume = {0.2, 0.3},
	pitch = {90, 110},
	level = 65,
	sound = {Sound")weapons/csgo/movement1.wav", Sound")weapons/csgo/movement2.wav", Sound")weapons/csgo/movement3.wav"}
})

SWEP.ItemDefAttributes = [=["attributes 09/07/2020"
{
	"bullets"		"8"
	"primary reserve ammo max"		"32"
	"primary clip size"		"6"
	"is full auto"		"0"

	"inaccuracy jump initial"		"0"
	"inaccuracy jump"		"0"

	"max player speed"		"240"
	"in game price"		"2500"

	"kill award"		"500"
	"armor ratio"		"0"

	"crosshair min distance"		"8"
	"crosshair delta distance"		"6"

	"penetration"		"1"
	"damage"		"30"

	"range"		"3000"
	"range modifier"		"0.700000"

	"cycletime"		"1"

	"flinch velocity modifier large"		"1"
	"flinch velocity modifier small"		"1"

	"spread"		"60.000000"

	"inaccuracy crouch"		"0"
	"inaccuracy stand"		"0"
	"inaccuracy land"		"0"
	"inaccuracy ladder"		"0"
	"inaccuracy fire"		"60"
	"inaccuracy move"		"0"

	"tracer frequency"			"1"

	"recovery time crouch"		"0.2"
	"recovery time stand"		"0.2"

	"recoil angle"		"0"
	"recoil angle variance"		"40"
	"recoil magnitude"		"150"
	"recoil magnitude variance"		"0"

	"recoil seed"		"7763"
	"spread seed"		"17514"

	"recovery time crouch final"		"0.2"
	"recovery time stand final"		"0.2"

	"kill award"		"300"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"muzzle_flash_effect_1st_person"		"weapon_muzzle_flash_autoshotgun"
	"muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_autoshotgun"
	"heat_effect"		"weapon_muzzle_smoke"
	"addon_location"		"primary_shotgun"
	"eject_brass_effect"		"weapon_shell_casing_shotgun"
	"tracer_effect"		"weapon_tracers_shot"
	"weapon_type"		"Shotgun"
	"player_animation_extension"		"nova"
	"primary_ammo"		"BULLET_PLAYER_BUCKSHOT"
	"sound_single_shot"		"Weapon_M45_CSGO.Single"
	"sound_nearlyempty"		"Default.nearlyempty"
}]=]

function SWEP:GetShotgunReloadState() return self:GetReloadState() end

function SWEP:SetupDataTables()
	BaseClass.SetupDataTables(self)

	self:NetworkVar("Int", "ReloadState")
end

function SWEP:Reload()
	if not self.m_bProcessingActivities then return end

	local owner = self:GetPlayerOwner()
	if not owner then
		return false end

	if self:GetAmmoCount(self:GetPrimaryAmmoType()) <= 0 or self:Clip1() >= self:GetMaxClip1() then
		return true end

	-- don't reload until recoil is done
	if self:GetNextPrimaryFire() > CurTime() then
		return true end

	-- check to see if we're ready to reload
	if self:GetReloadState() == 0 then
		self:SetInReload(true)
		self:SetFinishReloadTime(math.huge)
		owner:SetAnimation(PLAYER_RELOAD)

		self:SetWeaponAnim(ACT_SHOTGUN_RELOAD_START)
		self:SetReloadState(1)
		self:SetWeaponIdleTime(CurTime() + 0.5)
		self:SetNextPrimaryFire(CurTime() + 0.5)
		self:SetNextSecondaryFire(CurTime() + 0.5)
	elseif self:GetReloadState() == 1 then
		if self:GetWeaponIdleTime() > CurTime() then
			return true end
		-- was waiting for gun to move to side
		self:SetReloadState(2)

		self:SetWeaponAnim(ACT_VM_RELOAD)
		self:SetWeaponIdleTime(CurTime() + 0.5)
	elseif self:Clip1() < self:GetMaxClip1() then
		-- Add them to the clip
		self:SetClip1(self:Clip1() + 1)
		if SWCS_INDIVIDUAL_AMMO:GetBool() then
			self:SetReserveAmmo(self:GetReserveAmmo() - 1)
		else
			owner:RemoveAmmo(1, self:GetPrimaryAmmoType())
		end

		self:SetReloadState(1)
	end

	return true
end

function SWEP:WeaponIdle()
	local owner = self:GetPlayerOwner()
	if not owner then
		return end

	if self:GetWeaponIdleTime() < CurTime() then
		if self:Clip1() == 0 and self:GetReloadState() == 0 and self:GetAmmoCount(self:GetPrimaryAmmoType()) > 0 then
			self:Reload()
		elseif self:GetReloadState() ~= 0 then
			if self:Clip1() < self:GetMaxClip1() and self:GetAmmoCount(self:GetPrimaryAmmoType()) > 0 then
				self:Reload()
			else
				-- reload debounce has timed out
				self:SetInReload(false)
				self:SetWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
				self:SetReloadState(0)
				self:SetWeaponIdleTime(CurTime() + 1.5)
			end
		else
			self:SetWeaponAnim(ACT_VM_IDLE)
		end
	end
end

function SWEP:Holster(...)
	self:SetReloadState(0)
	self:SetInReload(false)

	return BaseClass.Holster(self, ...)
end

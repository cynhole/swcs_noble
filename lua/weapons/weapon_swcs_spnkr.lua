SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "M41 SPNKR"
SWEP.Spawnable = true
SWEP.HoldType = "rpg"
SWEP.WorldModel = Model"models/weapons/csgo/w_eq_spnkr.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_eq_spnkr.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/spnkr.png", "smooth")
	swcs.AddKillicon("weapon_swcs_spnkr", "hud/swcs/select/spnkr.png")
end

SWEP.Primary.Ammo = "RPG_Round"

SWEP.Slot = 4

if swcs.InTTT then
	SWEP.AutoSpawnable = false

	SWEP.Slot = 6

	SWEP.CanBuy = {ROLE_TRAITOR} -- only traitors can buy

	SWEP.IsSilent = false
	SWEP.Kind = WEAPON_EQUIP

	SWEP.Primary.Ammo = "AR2"
	SWEP.AmmoEnt = nil

	SWEP.LimitedStock = true
	SWEP.AllowDrop = true

	if CLIENT then
		--SWEP.Icon = "vgui/ttt/swcs_awp"
		SWEP.EquipMenuData = {
			type = "item_weapon",
			desc = "A risky way to clear out groups in one shot."
		}
	end
end

sound.Add({
	name = "Weapon_SPNKR_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 1.0,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/spnkr/spnkr-1.wav", Sound")weapons/csgo/spnkr/spnkr-2.wav", Sound")weapons/csgo/spnkr/spnkr-3.wav", Sound")weapons/csgo/spnkr/spnkr-4.wav"}
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.Draw",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {90, 110},
	sound = {Sound")weapons/csgo/spnkr/spnkr_draw1.wav", Sound")weapons/csgo/spnkr/spnkr_draw2.wav", Sound")weapons/csgo/spnkr/spnkr_draw3.wav"}
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.CoverUp",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {95, 105},
	sound = Sound")weapons/csgo/spnkr/spnkr_reload_coverup.wav"
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.CoverDown",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {95, 105},
	sound = {
		Sound")weapons/csgo/spnkr/spnkr_reload_coverdown1.wav",
		Sound")weapons/csgo/spnkr/spnkr_reload_coverdown2.wav",
		Sound")weapons/csgo/spnkr/spnkr_reload_coverdown3.wav"
	}
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.ReloadEnd",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	pitch = {92, 108},
	sound = Sound")weapons/csgo/spnkr/spnkr_reload_end.wav"
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.Remove",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/spnkr/spnkr_reload_remove1.wav", Sound")weapons/csgo/spnkr/spnkr_reload_remove2.wav"}
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.Place",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/spnkr/spnkr_reload_place1.wav", Sound")weapons/csgo/spnkr/spnkr_reload_place2.wav"}
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.Rotate",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/spnkr/spnkr_rotate1.wav", Sound")weapons/csgo/spnkr/spnkr_rotate2.wav", Sound")weapons/csgo/spnkr/spnkr_rotate3.wav"}
})
sound.Add({
	name = "Weapon_SPNKR_CSGO.Spin",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/spnkr/spnkr_spin1.wav", Sound")weapons/csgo/spnkr/spnkr_spin2.wav", Sound")weapons/csgo/spnkr/spnkr_spin3.wav"}
})

SWEP.ItemDefAttributes = [=["attributes 09/07/2020"
{
	"magazine model"		"models/weapons/w_eq_spnkr_mag.mdl"

	"tracer frequency"			"0"
	"primary clip size"			"]=] .. tostring(swcs.InTTT and 1 or 2) .. [=["
	"is full auto"				"0"

	"max player speed"			"240"

	"in game price"				"6500"
	"cycletime"					"0.6"

	"damage"					"0"
	"range"						"0"
	"range modifier"			"0"
	"bullets"					"0"
	"spread"					"30.000000"

	"inaccuracy crouch"			"0.0"
	"inaccuracy stand"			"0.0"
	"inaccuracy jump initial"	"0.0"
	"inaccuracy jump apex"		"0.0"
	"inaccuracy jump"			"0.0"
	"inaccuracy move"			"0.0"
	"inaccuracy land"			"0.0"
	"inaccuracy ladder"			"0.0"
	"inaccuracy fire"			"600.0"

	"recoil magnitude"			"100.0"
	"recoil magnitude variance"	"30"
	"primary reserve ammo max"	"4"

	"recovery time crouch"			"0.1"
	"recovery time stand"			"0.1"
	"recovery time crouch final" 	"0.1"
	"recovery time stand final" 	"0.1"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"muzzle_flash_effect_1st_person"		"spnkr_muzzle"
	"muzzle_flash_effect_3rd_person"		"spnkr_muzzle"
	"eject_brass_effect"		""
	"sound_single_shot"		"Weapon_SPNKR_CSGO.Single"
	"weapon_type"           "rifle"
}]=]

function SWEP:PrimaryAttackAct()
	if self:Clip1() > 1 then
		return ACT_VM_PRIMARYATTACK
	else
		return ACT_VM_DRYFIRE
	end
end

function SWEP:OnPrimaryAttack()
	local owner = self:GetOwner()
	if not owner:IsValid() then return end

	if SERVER then
		local proj = ents.Create("swcs_spnkr_projectile")
		local dir = owner:EyeAngles():Forward()
		proj:Create(owner:EyePos() + (dir * 50), dir, owner)
		proj:Spawn()

		hook.Run("PlayerSpawnedSENT", owner, proj)

		if swcs.InTTT and self:Clip1() == 0 then
			SafeRemoveEntity(self)
		end
	end
end

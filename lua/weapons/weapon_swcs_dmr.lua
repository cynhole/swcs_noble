SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "DMR"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_rif_dmr.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_rif_dmr.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/dmr.png", "smooth")
	swcs.AddKillicon("weapon_swcs_dmr", "hud/swcs/select/dmr.png")
end

SWEP.Slot = 2

sound.Add({
	name = "Weapon_DMR_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 0.95,
	sound = {Sound")weapons/csgo/dmr/dmr-1.wav", Sound")weapons/csgo/dmr/dmr-2.wav", Sound")weapons/csgo/dmr/dmr-3.wav"}
})
sound.Add({
	name = "Weapon_DMR_CSGO.ZoomIn",
	channel = CHAN_STATIC,
	level = 65,
	volume = 0.5,
	sound = Sound")weapons/csgo/dmr/dmr_zoomin.wav"
})
sound.Add({
	name = "Weapon_DMR_CSGO.ZoomOut",
	channel = CHAN_STATIC,
	level = 65,
	volume = 0.5,
	sound = Sound")weapons/csgo/dmr/dmr_zoomout.wav"
})
sound.Add({
	name = "Weapon_DMR_CSGO.Draw",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.65,
	pitch = {90, 110},
	sound = {Sound")weapons/csgo/dmr/dmr_draw.wav", Sound")weapons/csgo/dmr/dmr_draw2.wav"}
})
sound.Add({
	name = "Weapon_DMR_CSGO.BoltPull",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/dmr/dmr_boltpull.wav"
})
sound.Add({
	name = "Weapon_DMR_CSGO.BoltRelease",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/dmr/dmr_boltrelease.wav"
})
sound.Add({
	name = "Weapon_DMR_CSGO.ClipOut",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/dmr/dmr_clipout.wav"
})
sound.Add({
	name = "Weapon_DMR_CSGO.ClipIn",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/dmr/dmr_clipin.wav"
})
sound.Add({
	name = "Weapon_DMR_CSGO.ClipSlap",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/dmr/dmr_clipslap.wav"
})

SWEP.ItemDefAttributes = [=["attributes 11/02/2021"
{
	"in game price"		"3500"

	"magazine model"		"models/weapons/w_rif_dmr_mag.mdl"

	"aimsight capable"		"1"
	"aimsight speed up"		"10.000000"
	"aimsight speed down"		"8.000000"
	"aimsight looseness"		"0.030000"
	"aimsight eye pos"		"-1.56 -3.6 -0.07"
	"aimsight pivot angle"		"0.79 -0.107 -0.03"
	"aimsight fov"		"45"
	"aimsight pivot forward"		"10"
	"aimsight lens mask"		"models/weapons/csgo/v_rif_dmr_mask.mdl"
	"aimsight material"         "null"

	"primary reserve ammo max"		"60"
	"tracer frequency alt"		"1"
	"tracer frequency"			"1"
	"inaccuracy jump initial"		"0"
	"inaccuracy jump"		"0"
	"inaccuracy jump alt"		"0.0"
	"heat per shot"		"0.350000"
	"addon scale"		"0.900000"
	"is full auto"		"0"
	"armor ratio"		"1.900000"
	"crosshair min distance"		"3"
	"zoom levels"		"1"
	"zoom time 0"		"0.060000"
	"zoom fov 1"		"45"
	"zoom time 1"		"0.100000"
	"penetration"		"2"
	"damage"		"74"	// 43
	"range"		"8192"
	"cycletime"		"0.4"
	"time to idle"		"1.900000"
	"spread"		"0.500000"
	"inaccuracy crouch"		"4.00"
	"inaccuracy stand"		"4.00"
	"inaccuracy land"		"0.0"
	"inaccuracy ladder"		"0.0"
	"inaccuracy fire"		"45.0"
	"inaccuracy move"		"0.0"
	"spread alt"		"0.300000"
	"inaccuracy crouch alt"		"0"
	"inaccuracy stand alt"		"0"
	"inaccuracy land alt"		"0"
	"inaccuracy ladder alt"		"0"
	"inaccuracy fire alt"		"26.0"
	"inaccuracy move alt"		"0"
	"recovery time crouch"		"0.3"
	"recovery time stand"		"0.4"
	"recoil angle"		"0"
	"recoil angle variance"		"40"
	"recoil magnitude"		"45"
	"recoil magnitude variance"		"0"
	"recoil seed"		"24204"
	"recoil magnitude alt"		"16"
	"primary clip size"		"15"
	"weapon weight"		"25"
	"rumble effect"		"4"
	"recoil angle alt"		"0"
	"recoil angle variance alt"		"60"
	"recoil magnitude variance alt"		"0"
	"recovery time crouch final"		"0.21"
	"recovery time stand final"		"0.42"
	"range modifier"		"0.970000"

	"icon display model"		"models/weapons/w_rif_dmr.mdl"
	"pedestal display model"		"models/weapons/w_rif_dmr_inspect.mdl"

	// Specific
	"armor ratio"		"0"
	"flinch velocity modifier large"		"1"
	"flinch velocity modifier small"		"1"

	"max player speed"		"240"
	"max player speed alt"		"240"

	"kill award"		"400"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"muzzle_flash_effect_1st_person"		"dmr_muzzle"
	"muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_huntingrifle"
	"heat_effect"		"weapon_muzzle_smoke"
	"addon_location"		"primary_rifle"
	"eject_brass_effect"		"weapon_shell_casing_rifle"
	"tracer_effect"		"weapon_tracers_assrifle"
	"player_animation_extension"		"aug"
	"primary_ammo"		"BULLET_PLAYER_762MM"
	"sound_single_shot"		"Weapon_DMR_CSGO.Single"
	"sound_nearlyempty"		"Default.nearlyempty"
	"weapon_type" "Rifle"
}]=]
SWEP.ItemDefPrefab = [=["prefab 09/07/2020" {
	"zoom_in_sound"		"Weapon_DMR_CSGO.ZoomIn"
	"zoom_out_sound"		"Weapon_DMR_CSGO.ZoomOut"
}]=]

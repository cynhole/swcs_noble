SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "Covenant Carbine"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_rif_covenant_carbine.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_rif_covenant_carbine.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/carbine.png", "smooth")
	swcs.AddKillicon("weapon_swcs_carbine", "hud/swcs/select/carbine.png")
end

SWEP.Slot = 2

sound.Add({
	name = "Weapon_Carbine_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 0.8,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/covenant_carbine/carbine-1.wav", Sound")weapons/csgo/covenant_carbine/carbine-2.wav", Sound")weapons/csgo/covenant_carbine/carbine-3.wav"}
})
sound.Add({
	name = "Weapon_Carbine_CSGO.ZoomIn",
	channel = CHAN_STATIC,
	level = 65,
	volume = 0.7,
	sound = Sound")weapons/csgo/covenant_carbine/carbine_zoomin.wav"
})
sound.Add({
	name = "Weapon_Carbine_CSGO.ZoomOut",
	channel = CHAN_STATIC,
	level = 65,
	volume = 0.7,
	sound = Sound")weapons/csgo/covenant_carbine/carbine_zoomout.wav"
})
sound.Add({
	name = "Weapon_Carbine_CSGO.Ready",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.65,
	pitch = {90, 110},
	sound = {Sound")weapons/csgo/covenant_carbine/carbine_ready1.wav", Sound")weapons/csgo/covenant_carbine/carbine_ready2.wav", Sound")weapons/csgo/covenant_carbine/carbine_ready3.wav"}
})
sound.Add({
	name = "Weapon_Carbine_CSGO.ClipOut",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.8,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/covenant_carbine/carbine_clipout.wav"
})
sound.Add({
	name = "Weapon_Carbine_CSGO.ClipIn",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.8,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/covenant_carbine/carbine_clipin.wav"
})
sound.Add({
	name = "Weapon_Carbine_CSGO.Open",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.4,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/covenant_carbine/carbine_open.wav"
})
sound.Add({
	name = "Weapon_Carbine_CSGO.Close",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.4,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/covenant_carbine/carbine_close.wav"
})

SWEP.ItemDefAttributes = [=["attributes 11/02/2021"
{
	"in game price"		"2800"

	"aimsight capable"		"1"
	"aimsight speed up"		"10.000000"
	"aimsight speed down"		"8.000000"
	"aimsight looseness"		"0.030000"
	"aimsight eye pos"		"0.72 -5.12 -1.33"
	"aimsight pivot angle"		"0.52 0.04 0.72"
	"aimsight fov"		"45"
	"aimsight pivot forward"		"8"
	"aimsight lens mask"		"models/weapons/csgo/v_rif_covenant_carbine_mask.mdl"
	"aimsight material"         "null"

	"zoom levels"		"1"
	"zoom time 0"		"0.060000"
	"zoom fov 1"		"45"
	"zoom time 1"		"0.100000"
	"penetration"		"2"
	"tracer frequency alt"		"1"
	"tracer frequency"			"1"

	"magazine model"		"models/weapons/w_rif_covenant_carbine_mag.mdl"
	"tracer frequency alt"		"1"
	"tracer frequency"			"1"
	"is full auto"				"0"
	"inaccuracy jump initial"		"0.0"
	"inaccuracy jump"		"0.0"
	"inaccuracy jump alt"		"0.0"
	"armor ratio"		"1.5"
	"crosshair min distance"		"3"
	"penetration"		"0"
	"damage"		"40"	// 45
	"range"		"8192"
	"cycletime"		"0.24"
	"time to idle"		"1.5"
	"spread"		"0.15"
	"spread alt"	"0.075"
	"inaccuracy crouch"		"7.0"
	"inaccuracy stand"		"7.0"
	"inaccuracy land"		"0"
	"inaccuracy ladder"		"0"
	"inaccuracy fire"		"12.0"
	"inaccuracy move"		"0"
	"inaccuracy crouch alt"		"0.0"
	"inaccuracy stand alt"		"0.0"
	"inaccuracy land alt"		"0"
	"inaccuracy ladder alt"		"0"
	"inaccuracy fire alt"		"6.0"
	"inaccuracy move alt"		"0"
	"recovery time crouch"		"0.3"
	"recovery time stand"		"0.3"
	"recoil angle"		"0"
	"recoil angle alt"		"0"
	"recoil angle variance"		"10"
	"recoil angle variance alt"		"10"
	"recoil magnitude"		"18"
	"recoil magnitude alt"		"10"
	"recoil magnitude variance"		"8"
	"recoil magnitude variance alt"		"8"
	"recoil seed"		"24204"
	"weapon weight"		"25"
	"rumble effect"		"4"
	"primary clip size"			"18"
	"primary reserve ammo max"	"54"
	"recovery time crouch final"		"0.42"
	"recovery time stand final"		"0.42"

	"icon display model"		"models/weapons/w_rif_covenant_carbine.mdl"
	"pedestal display model"		"models/weapons/w_rif_covenant_carbine_inspect.mdl"

	// Specific
	"armor ratio"		"-2"
	"flinch velocity modifier large"		"1"
	"flinch velocity modifier small"		"1"

	"max player speed"		"240"
	"max player speed alt"		"240"

	"kill award"		"400"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"primary_ammo"		"BULLET_PLAYER_357SIG_MIN"
	"muzzle_flash_effect_1st_person"		"carbine_muzzle"
	"muzzle_flash_effect_3rd_person"		"carbine_muzzle"
	"heat_effect"		"weapon_muzzle_smoke"
	"eject_brass_effect"		""
	"tracer_effect"		"carbine_tracer"
	"sound_single_shot"		"Weapon_Carbine_CSGO.Single"
	"sound_nearlyempty"		"Default.nearlyempty"
	"weapon_type" "Rifle"
}]=]
SWEP.ItemDefPrefab = [=["prefab 09/07/2020" {
	"zoom_in_sound"		"Weapon_Carbine_CSGO.ZoomIn"
	"zoom_out_sound"		"Weapon_Carbine_CSGO.ZoomOut"
}]=]

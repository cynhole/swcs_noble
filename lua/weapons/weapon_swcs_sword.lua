SWEP.Base = "weapon_swcs_knife"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

-- NOTE: TODO:
-- In Noble Strike, the energy sword is throwable???
-- probably due to the fact it overwrites a danger-zone melee wep
SWEP.PrintName = "Energy Sword"
SWEP.Spawnable = true
SWEP.HoldType = "knife"
SWEP.WorldModel = Model"models/weapons/csgo/w_knife_energy_sword.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_knife_energy_sword.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/energy_sword.png", "smooth")
	swcs.AddKillicon("weapon_swcs_sword", "hud/swcs/select/energy_sword.png")
end

SWEP.ItemDefAttributes = [=["attributes 08/31/2020" {
	"primary clip size" "-1"
	"is full auto" "1"
	"recoil seed" "0"
	"recoil angle variance" "0"
	"recoil magnitude" "0"
	"recoil magnitude variance" "0"
	"recoil angle variance alt" "0"
	"recoil magnitude alt" "0"
	"recoil magnitude variance alt" "0"
}]=]
SWEP.ItemDefVisuals = [=["visuals 08/31/2020" {
	"weapon_type" "knife"
}]=]

SWEP.IsKnife = true
SWEP.IsSWCSWeapon = true

SWEP.AutoSpawnable = false -- ttt

sound.Add({
	name = "Weapon_EnergySword_CSGO.Deploy",
	channel = CHAN_WEAPON,
	volume = 0.1,
	pitch = {95, 105},
	level = 65,
	sound = Sound")weapons/csgo/energy_sword/sword-deploy.wav"
})
sound.Add({
	name = "Weapon_EnergySword_CSGO.Hit",
	channel = CHAN_STATIC,
	volume = {0.15, 0.25},
	pitch = {120, 150},
	level = 65,
	sound = {
		Sound")weapons/csgo/energy_sword/impact/sword-hit1.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit2.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit3.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit4.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit5.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit6.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit7.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit8.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit9.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit10.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit11.wav"
	}
})
sound.Add({
	name = "Weapon_EnergySword_CSGO.HitHard",
	channel = CHAN_STATIC,
	volume = {0.20, 0.28},
	pitch = {120, 150},
	level = 65,
	sound = {
		Sound")weapons/csgo/energy_sword/impact/sword-hit1.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit2.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit3.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit4.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit5.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit6.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit7.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit8.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit9.wav", Sound")weapons/csgo/energy_sword/impact/sword-hit10.wav",
		Sound")weapons/csgo/energy_sword/impact/sword-hit11.wav"
	}
})
sound.Add({
	name = "Weapon_EnergySword_CSGO.Swing_Left",
	channel = CHAN_WEAPON,
	volume = {0.2, 0.3},
	pitch = {97, 105},
	level = 65,
	sound = {Sound")weapons/csgo/energy_sword/left/sword-swing1.wav", Sound")weapons/csgo/energy_sword/left/sword-swing2.wav", Sound")weapons/csgo/energy_sword/left/sword-swing3.wav"}
})
sound.Add({
	name = "Weapon_EnergySword_CSGO.Swing_Right",
	channel = CHAN_WEAPON,
	volume = {0.2, 0.3},
	pitch = {97, 105},
	level = 65,
	sound = {Sound")weapons/csgo/energy_sword/left/sword-swing1.wav", Sound")weapons/csgo/energy_sword/left/sword-swing2.wav", Sound")weapons/csgo/energy_sword/left/sword-swing3.wav"}
})
sound.Add({
	name = "Weapon_EnergySword_CSGO.HeavySwing",
	channel = CHAN_WEAPON,
	volume = {0.22, 0.33},
	pitch = {97, 105},
	level = 65,
	sound = {Sound")weapons/csgo/energy_sword/sword-swing_hard1.wav", Sound")weapons/csgo/energy_sword/sword-swing_hard2.wav", Sound")weapons/csgo/energy_sword/sword-swing_hard3.wav"}
})
sound.Add({
	name = "Weapon_EnergySword_CSGO.WeaponMove",
	channel = CHAN_ITEM,
	volume = {0.3, 0.4},
	pitch = {105, 110},
	level = 65,
	sound = {Sound")weapons/csgo/movement1.wav", Sound")weapons/csgo/movement2.wav", Sound")weapons/csgo/movement3.wav"}
})

local MAX_RANGE = 300
local MIN_RANGE = 90
local LUNGE_SPEED = 600
local LUNGE_ANG_UP_OFFSET = 50

function SWEP:LaunchPlayer()
	local owner = self:GetPlayerOwner()
	if not owner then
		return false end

	local tr = util.TraceHull({
		start = owner:GetShootPos(),
		endpos = owner:GetShootPos() + (owner:GetAimVector() * MAX_RANGE),
		mins = -Vector(16,16,16), maxs = Vector(16,16,16),
		mask = MASK_SHOT, filter = owner,
	})

	local target = tr.Entity
	if not (target:IsValid() and (target:IsPlayer() or target:IsNPC() or target:IsNextBot())) then
		return false end

	local flDist = (target:GetPos() - owner:GetPos()):Length()
	if flDist < MIN_RANGE then
		return false end

	-- Lunge
	local lungeDir = target:EyePos() - owner:EyePos() + Vector(0,0, LUNGE_ANG_UP_OFFSET)
	lungeDir:Normalize()

	owner:SetGroundEntity(NULL)

	local vel = owner:GetVelocity()
	vel:Mul(-1)
	vel:Add(lungeDir * LUNGE_SPEED * ((flDist / MAX_RANGE) + 0.2))
	owner:SetVelocity(vel)

	return true
end

function SWEP:PrimaryAttack()
	BaseClass.PrimaryAttack(self)

	if self:LaunchPlayer() then
		self:SetWeaponAnim(ACT_VM_MISSCENTER2)
	end
end

function SWEP:SecondaryAttack()
	BaseClass.SecondaryAttack(self)

	--self:LaunchPlayer()
end

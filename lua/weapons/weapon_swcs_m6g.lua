SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "M6G Magnum"
SWEP.Spawnable = true
SWEP.HoldType = "pistol"
SWEP.WorldModel = Model"models/weapons/csgo/w_pist_m6g.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_pist_m6g.mdl"
SWEP.TTTIsDeagle = true
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/m6g.png", "smooth")
	swcs.AddKillicon("weapon_swcs_m6g", "hud/swcs/select/m6g.png")
end

sound.Add({
	name = "Weapon_M6G_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 0.8,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/m6g/m6g-1.wav", Sound")weapons/csgo/m6g/m6g-2.wav", Sound")weapons/csgo/m6g/m6g-3.wav", Sound")weapons/csgo/m6g/m6g-4.wav"}
})
sound.Add({
	name = "Weapon_M6G_CSGO.Draw",
	channel = CHAN_STATIC,
	level = 65,
	volume = 0.3,
	sound = Sound")weapons/csgo/deagle/de_draw.wav"
})
sound.Add({
	name = "Weapon_M6G_CSGO.ClipOut",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	sound = Sound")weapons/csgo/m6g/m6g_clipout.wav"
})
sound.Add({
	name = "Weapon_M6G_CSGO.ClipIn",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	sound = Sound")weapons/csgo/m6g/m6g_clipin.wav"
})
sound.Add({
	name = "Weapon_M6G_CSGO.Slideback",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	sound = Sound")weapons/csgo/m6g/m6g_slideback.wav"
})
sound.Add({
	name = "Weapon_M6G_CSGO.Slideforward",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	sound = Sound")weapons/csgo/m6g/m6g_slideforward.wav"
})
sound.Add({
	name = "Weapon_M6G_CSGO.WeaponMove1",
	channel = CHAN_ITEM,
	level = 65,
	volume = {0.05, .1},
	pitch = {98, 101},
	sound = Sound"weapons/csgo/movement1.wav"
})
sound.Add({
	name = "Weapon_M6G_CSGO.WeaponMove3",
	channel = CHAN_ITEM,
	level = 65,
	volume = {0.05, .1},
	pitch = {98, 101},
	sound = Sound"weapons/csgo/movement3.wav"
})
sound.Add({
	name = "Weapon_M6G_CSGO.WeaponMove4",
	channel = CHAN_ITEM,
	level = 65,
	volume = {0.05, .1},
	pitch = {98, 101},
	sound = Sound"weapons/csgo/movement2.wav"
})

SWEP.ItemDefAttributes = [=["attributes 09/07/2020"
{
	"in game price"		"1000"

	"is full auto" "0"
	"primary clip size"		"8"
	"primary reserve ammo max" "48"
	"recoil angle"		"0"
	"recoil angle variance"		"10"
	"recoil magnitude"		"15"
	"recoil magnitude variance"		"0"
	"cycletime" "0.25"
	"damage" "56"	// 32
	"spread"		"2.000000"
	"inaccuracy crouch"			"1.000000"
	"inaccuracy stand"			"1.000000"
	"inaccuracy land"			"0.0"
	"inaccuracy ladder"			"0.0"
	"inaccuracy fire"			"23.0"
	"inaccuracy move"			"0.000000"
	"inaccuracy jump initial"	"0.0"
	"inaccuracy jump"			"0.0"
	"recovery time crouch"		"0.42"
	"recovery time stand"		"0.42"
	"tracer frequency alt"		"1"
	"tracer frequency"			"1"

	"range"     "8192"

	// Specific
	"armor ratio"		"0"
	"flinch velocity modifier large"		"1"
	"flinch velocity modifier small"		"1"

	"max player speed"		"240"
	"max player speed alt"		"240"

	"kill award"		"300"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"sound_single_shot" "Weapon_M6G_CSGO.Single"
	"sound_single_shot_accurate" "Weapon_M6G_CSGO.Single"
	"primary_ammo"		"BULLET_PLAYER_357SIG_P250"
	"muzzle_flash_effect_1st_person"		"magnum_muzzle"
	"weapon_type" "Pistol"
}]=]

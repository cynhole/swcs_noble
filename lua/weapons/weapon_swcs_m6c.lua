SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "M6C Pistol"
SWEP.Spawnable = true
SWEP.HoldType = "pistol"
SWEP.WorldModel = Model"models/weapons/csgo/w_pist_m6c.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_pist_m6c.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/m6c.png", "smooth")
	swcs.AddKillicon("weapon_swcs_m6c", "hud/swcs/select/m6c.png")
end

sound.Add({
	name = "Weapon_M6C_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 1.0,
	pitch = {103, 115},
	sound = {Sound")weapons/csgo/m6c/m6c-1.wav", Sound")weapons/csgo/m6c/m6c-2.wav"}
})
sound.Add({
	name = "Weapon_M6C_CSGO.Draw",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.16,
	pitch = 117,
	sound = Sound"weapons/csgo/deagle/de_draw.wav"
})
sound.Add({
	name = "Weapon_M6C_CSGO.ClipOut",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	sound = Sound"weapons/csgo/m6c/m6c_clipout.wav"
})
sound.Add({
	name = "Weapon_M6C_CSGO.ClipIn",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	sound = Sound"weapons/csgo/m6c/m6c_clipin.wav"
})
sound.Add({
	name = "Weapon_M6C_CSGO.Safety",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.6,
	sound = Sound")weapons/csgo/m6c/m6c_safety.wav"
})

SWEP.ItemDefAttributes = [=["attributes 09/07/2020"
{
	"in game price"		"250"

	"primary clip size"		"12"
	"primary reserve ammo max" "48"
	"recoil magnitude"		"8"
	"recoil magnitude variance"		"0"
	"cycletime" "0.16666666"
	"damage" "35"
	"inaccuracy crouch"			"5.000000"
	"inaccuracy stand"			"5.000000"
	"inaccuracy fire"			"15.0"
	"recovery time crouch"		"0.28"
	"recovery time stand"		"0.42"
	"tracer frequency"			"1"

	"range"     "8192"

	"max player speed"		"240"
	"max player speed alt"		"240"

	"kill award"		"400"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"sound_single_shot" "Weapon_M6C_CSGO.Single"
	"sound_single_shot_accurate" "Weapon_M6C_CSGO.Single"
	"primary_ammo"		"BULLET_PLAYER_357SIG_P250"
	"muzzle_flash_effect_1st_person"		"magnum_muzzle_m6c"
	"weapon_type" "Pistol"
}]=]

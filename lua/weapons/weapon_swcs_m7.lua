SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "M7 SMG"
SWEP.Spawnable = true
SWEP.HoldType = "smg"
SWEP.WorldModel = Model"models/weapons/csgo/w_smg_m7.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_smg_m7.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/m7.png", "smooth")
	swcs.AddKillicon("weapon_swcs_m7", "hud/swcs/select/m7.png")
end

SWEP.Slot = 2

sound.Add({
	name = "Weapon_M7_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 0.95,
	pitch = {97, 108},
	sound = {Sound")weapons/csgo/m7/m7-1.wav", Sound")weapons/csgo/m7/m7-2.wav", Sound")weapons/csgo/m7/m7-3.wav"}
})
sound.Add({
	name = "Weapon_M7_CSGO.Draw",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.45,
	pitch = {90, 110},
	sound = {Sound")weapons/csgo/m7/m7_draw1.wav", Sound")weapons/csgo/m7/m7_draw2.wav", Sound")weapons/csgo/m7/m7_draw3.wav"}
})
sound.Add({
	name = "Weapon_M7_CSGO.ClipOut",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.65,
	pitch = {90, 110},
	sound = {Sound")weapons/csgo/m7/m7_clipout1.wav", Sound")weapons/csgo/m7/m7_clipout2.wav"}
})
sound.Add({
	name = "Weapon_M7_CSGO.ClipIn",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.65,
	pitch = {90, 110},
	sound = Sound")weapons/csgo/m7/m7_clipin.wav"
})

SWEP.ItemDefAttributes = [=["attributes 09/07/2020"
{
	"in game price"		"1500"

	"is full auto" "1"

	"cycletime" "0.066666666"

	"damage" "23"

	"primary clip size"		"60"
	"primary reserve ammo max" "240"

	"spread seed"		"56"
	"recoil angle"				"0"
	"recoil angle variance"		"220"
	"recoil magnitude"			"13"

	"inaccuracy jump initial"	"0.0"
	"inaccuracy jump"			"0.0"
	"inaccuracy land"			"0.0"
	"inaccuracy ladder"			"0.0"
	"inaccuracy crouch"			"20.000000"
	"inaccuracy stand"			"20.000000"
	"inaccuracy fire"			"8.0"
	"inaccuracy move"			"0"
	"recovery time crouch"		"0.18"
	"recovery time stand"		"0.26"
	"tracer frequency"			"3"

	"max player speed"		"240"

	"range"		"8192"
	"range modifier"		"0.960000"

	// Specific
	"armor ratio"		"0"
	"flinch velocity modifier large"		"1"
	"flinch velocity modifier small"		"1"

	"kill award"		"250"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"sound_single_shot"		"Weapon_M7_CSGO.Single"
	"muzzle_flash_effect_1st_person"		"m7_muzzle"
	"primary_ammo"		"BULLET_PLAYER_357SIG_P250"
	"eject_brass_effect"	""
	"weapon_type" "SubMachinegun"
}]=]

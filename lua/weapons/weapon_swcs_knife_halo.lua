SWEP.Base = "weapon_swcs_knife"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "Reach Knife"
SWEP.Spawnable = true
SWEP.HoldType = "knife"
SWEP.WorldModel = Model"models/weapons/csgo/w_knife_reach.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_knife_reach.mdl"
if CLIENT then
    SWEP.SelectIcon = Material("hud/swcs/select/knife_halo.png", "smooth")
    swcs.AddKillicon("weapon_swcs_knife_halo", "hud/swcs/select/knife_halo.png")
end

SWEP.ItemDefAttributes = [=["attributes 08/31/2020" {
    "primary clip size" "-1"
    "is full auto" "1"
    "recoil seed" "0"
    "recoil angle variance" "0"
    "recoil magnitude" "0"
    "recoil magnitude variance" "0"
    "recoil angle variance alt" "0"
    "recoil magnitude alt" "0"
    "recoil magnitude variance alt" "0"
}]=]
SWEP.ItemDefVisuals = [=["visuals 08/31/2020" {
    "weapon_type" "knife"
}]=]

SWEP.IsKnife = true
SWEP.IsSWCSWeapon = true

SWEP.AutoSpawnable = false -- ttt

--if t.HoldType then
--    SWEP.HoldType = t.HoldType
--end

--weapons.Register(SWEP, classname)

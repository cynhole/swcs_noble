SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "Classic MA37 Assault Rifle"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_rif_ma37.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_rif_ma37_h3.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/ma37_c.png", "smooth")
	swcs.AddKillicon("weapon_swcs_ma37_c", "hud/swcs/select/ma37_c.png")
end

SWEP.Slot = 2

sound.Add({
	name = "Weapon_MA37_H3_CSGO.Single",
	channel = CHAN_STATIC,
	level = 79,
	volume = 0.9,
	pitch = {95, 105},
	sound = {Sound")weapons/csgo/ma37_h3/ma37-1.wav", Sound")weapons/csgo/ma37_h3/ma37-2.wav", Sound")weapons/csgo/ma37_h3/ma37-3.wav"}
})

function SWEP:Think()
	BaseClass.Think(self)

	local owner = self:GetPlayerOwner()
	if not owner then return end

	local vm = owner:GetViewModel(self:ViewModelIndex())
	if not vm:IsValid() then return end

	local angle = owner:EyeAngles().y - 90
	if angle < 0 then angle = angle + 360 end

	local index = math.ceil( ( (angle + 22.5) / 360 ) * 8 )
	if index > 8 then index = 1 end

	vm:SetSkin(index)
end

--function SWEP:CalcViewModelView(vm, opos, oang, pos, ang)
--	vm:SetPoseParameter("ammo", 1 - (self:Clip1() / self:GetMaxClip1()))
--
--	return BaseClass.CalcViewModelView(self, vm, opos, oang, pos, ang)
--end

SWEP.ItemDefAttributes = [=["attributes 11/02/2021"
{
	"in game price"		"3500"
	"magazine model"		"models/weapons/w_rif_dmr_mag.mdl"

	"recoil seed"		"38965"
	"recoil angle"		"0"
	"recoil angle variance"		"70"
	"recoil magnitude"		"23"
	"recoil magnitude variance"		"0"
	"recoil angle alt"		"0"
	"recoil angle variance alt"		"70"
	"recoil magnitude alt"		"23"
	"recoil magnitude variance alt"		"0"

	"is full auto" "1"
	"range" "8192"

	"primary clip size"		"32"
	"primary reserve ammo max"	"128"

	"recoil angle"				"0"
	"recoil angle variance"		"60"
	"recoil magnitude"			"12"

	"penetration"				"1"
	"cycletime"					"0.1"
	"damage"					"31"

	"spread"		"0.300000"
	"inaccuracy jump initial"	"0.0"
	"inaccuracy jump"			"0.0"
	"inaccuracy crouch"			"4.000000"
	"inaccuracy stand"			"4.000000"
	"inaccuracy land"			"0.0"
	"inaccuracy ladder"			"0.0"
	"inaccuracy fire"			"12.0"
	"inaccuracy move"			"0.0"
	"recovery time crouch"		"0.14"
	"recovery time stand"		"0.22"
	"tracer frequency"			"1"

	// Specific
	"armor ratio"		"0"
	"flinch velocity modifier large"		"1"
	"flinch velocity modifier small"		"1"

	"max player speed"		"240"
	"max player speed alt"		"240"

	"kill award"		"300"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"muzzle_flash_effect_1st_person"		"ma37_muzzle"
	"muzzle_flash_effect_3rd_person"		"weapon_muzzle_flash_assaultrifle"
	"heat_effect"		"weapon_muzzle_smoke"
	"addon_location"		"primary_rifle"
	"eject_brass_effect"		"weapon_shell_casing_rifle"
	"tracer_effect"		"weapon_tracers_assrifle"
	"player_animation_extension"		"aug"
	"primary_ammo"		"BULLET_PLAYER_762MM"
	"sound_single_shot"		"Weapon_MA37_H3_CSGO.Single"
	"sound_nearlyempty"		"Default.nearlyempty"
	"weapon_type" "Rifle"
}]=]

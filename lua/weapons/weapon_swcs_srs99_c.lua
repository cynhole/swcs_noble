SWEP.Base = "weapon_swcs_base"
SWEP.Category = "NOBLE Strike"

DEFINE_BASECLASS(SWEP.Base)

SWEP.PrintName = "Classic SRS99 Sniper Rifle"
SWEP.Spawnable = true
SWEP.HoldType = "ar2"
SWEP.WorldModel = Model"models/weapons/csgo/w_snip_srs99.mdl"
SWEP.ViewModel = Model"models/weapons/csgo/v_snip_srs99_h3.mdl"
if CLIENT then
	SWEP.SelectIcon = Material("hud/swcs/select/srs99_c.png", "smooth")
	swcs.AddKillicon("weapon_swcs_srs99_c", "hud/swcs/select/srs99_c.png")
end

SWEP.Slot = 3

sound.Add({
	name = "Weapon_SRS99_H3_CSGO.Boltpull",
	channel = CHAN_ITEM,
	level = 65,
	volume = 1,
	pitch = {97, 102},
	sound = Sound"weapons/csgo/srs99_h3/srs99_boltpull.wav"
})
sound.Add({
	name = "Weapon_SRS99_H3_CSGO.Boltrelease",
	channel = CHAN_ITEM,
	level = 65,
	volume = 1,
	pitch = {97, 102},
	sound = Sound"weapons/csgo/srs99_h3/srs99_boltrelease.wav"
})
sound.Add({
	name = "Weapon_SRS99_H3_CSGO.Clipout",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {90, 110},
	sound = {Sound"weapons/csgo/srs99_h3/srs99_clipout1.wav", Sound"weapons/csgo/srs99_h3/srs99_clipout2.wav", Sound"weapons/csgo/srs99_h3/srs99_clipout3.wav"}
})
sound.Add({
	name = "Weapon_SRS99_H3_CSGO.Clipin",
	channel = CHAN_ITEM,
	level = 65,
	volume = 0.7,
	pitch = {90, 110},
	sound = {Sound"weapons/csgo/srs99_h3/srs99_clipin1.wav", Sound"weapons/csgo/srs99_h3/srs99_clipin2.wav", Sound"weapons/csgo/srs99_h3/srs99_clipin3.wav"}
})

SWEP.ItemDefAttributes = [=["attributes 09/07/2020"
{
	"icon display model"		"models/weapons/w_snip_srs99.mdl"
	"pedestal display model"		"models/weapons/w_snip_srs99_inspect.mdl"

	"scope overlay" "overlays/scope_lens_srs"
	"scope arc" "null"

	"tracer frequency alt"		"1"
	"tracer frequency"			"1"

	"magazine model"		"models/weapons/w_snip_srs99_mag.mdl"
	"unzoom after shot"		"0"
	"primary reserve ammo max"		"20"

	"inaccuracy jump initial"	"0.0"
	"inaccuracy jump"			"0.0"
	"inaccuracy jump alt"		"0.0"

	"heat per shot"		"1.500000"
	"addon scale"		"0.900000"

	"max player speed"		"240"
	"max player speed alt"		"240"

	"in game price"		"4000"

	"armor ratio"		"0.0"

	"zoom levels"		"2"
	"zoom time 0"		"0"
	"zoom fov 1"		"30"
	"zoom fov 2"		"9"
	"zoom time 1"		"0.1"
	"zoom time 2"		"0.1"

	"penetration"		"2.500000"

	"damage"		"175"

	"range"		"8192"
	"range modifier"		"0.990000"

	"cycletime"		"0.8"

	"flinch velocity modifier large"		"1"
	"flinch velocity modifier small"		"1"

	"inaccuracy reload"		"0"

	"spread"		"0.0"

	"hide view model zoomed"		"1"

	"inaccuracy crouch"		"0.0"
	"inaccuracy stand"		"0.0"
	"inaccuracy land"		"0.0"
	"inaccuracy ladder"		"0.0"
	"inaccuracy fire"		"40.0"
	"inaccuracy move"		"0.0"

	"spread alt"		"0.0"

	"inaccuracy crouch alt"		"0.0"
	"inaccuracy stand alt"		"0.0"
	"inaccuracy land alt"		"0.0"
	"inaccuracy ladder alt"		"0.0"
	"inaccuracy fire alt"		"40.0"
	"inaccuracy move alt"		"0.0"
	"recovery time crouch"		"0.246710"
	"recovery time stand"		"0.345390"

	"recoil angle"		"0"
	"recoil angle variance"		"5"

	"recoil magnitude"		"50"
	"recoil magnitude variance"		"0"
	"recoil seed"		"4100"

	"primary clip size"		"4"
	"weapon weight"		"30"
	"rumble effect"		"2"

	"recoil angle alt"		"0"
	"recoil angle variance alt"		"20"
	"recoil magnitude alt"		"30"
	"recoil magnitude variance alt"		"0"

	"recovery time crouch final"		"0.2"
	"recovery time stand final"		"0.2"

	"kill award"		"200"
}]=]
SWEP.ItemDefVisuals = [=["visuals 09/07/2020"
{
	"muzzle_flash_effect_1st_person"		"srs99_muzzle"
	"muzzle_flash_effect_3rd_person"		"srs99_muzzle"
	"heat_effect"		"weapon_muzzle_smoke"
	"addon_location"		"primary_sniper"
	"eject_brass_effect"		"weapon_shell_casing_50cal"
	"weapon_type"		"SniperRifle"
	"player_animation_extension"		"awp"
	"primary_ammo"		"BULLET_PLAYER_338MAG"
	"sound_single_shot"		"Weapon_SRS99_CSGO.Single"
	"sound_nearlyempty"		"Default.nearlyempty"
	"tracer_effect"		"srs99_tracer"
}]=]
SWEP.ItemDefPrefab = [=["prefab 09/07/2020" {
	"zoom_in_sound"		"Weapon_SRS99_CSGO.ZoomIn"
	"zoom_out_sound"		"Weapon_SRS99_CSGO.ZoomOut"
}]=]

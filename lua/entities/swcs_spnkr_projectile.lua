AddCSLuaFile()
ENT.Type = "anim"

local movespeed = 1100

local exp_magnitude = 320
local exp_radius = 240

local PROJ_MODEL = "models/weapons/csgo/w_eq_spnkr_rocket.mdl"
if CLIENT then
	swcs.AddKillicon("swcs_spnkr_projectile", "hud/swcs/select/spnkr.png")
end

sound.Add({
	name = "Weapon_SPNKR.Travel",
	channel = CHAN_BODY,
	volume = 0.8,
	level = 70,
	pitch = 100,
	sound = Sound"weapons/csgo/spnkr/spnkr_travel.wav"
})

ENT.m_iSoundTravelID = -1
function ENT:Initialize()
	self:SetModel(PROJ_MODEL)
	self:PhysicsInitBox(Vector(-1, -1, -1), Vector(1, 1, 1))
	self:SetMoveType(MOVETYPE_FLY)
	self:SetMoveCollide(MOVECOLLIDE_FLY_CUSTOM)

	self:AddFlags(FL_GRENADE)

	self:EmitSound("Weapon_SPNKR.Travel")
	--self.m_iSoundTravelID = self:StartLoopingSound("Weapon_SPNKR.Travel")
end

function ENT:Create(pos, dir, owner)
	self:SetPos(owner:EyePos())
	self:SetAngles(dir:Angle())
	self:SetOwner(owner)

	self:SetVelocity(dir * movespeed)
end

function ENT:Touch(other)
	if self.m_iSoundTravelID ~= -1 then
		self:StopLoopingSound(self.m_iSoundTravelID)
	end

	local tr = self:GetTouchTrace()

	self:Explode(tr)
	self:StopSound("Weapon_SPNKR.Travel")
	self:EmitSound("HEGrenade.Explode")

	self:Remove()
end

local MAX_WATER_SURFACE_DISTANCE = 512
function ENT:Explode(tr)
	self:AddSolidFlags(FSOLID_NOT_SOLID)
	if SERVER then self:SetSaveValue("m_takedamage", 0) end

	local vecReported = self:GetOwner():IsValid() and self:GetOwner():GetPos() or vector_origin

	if SERVER then
		local info = DamageInfo()
		info:SetInflictor(self)
		info:SetAttacker(self:GetOwner():IsValid() and self:GetOwner() or self)
		--info:SetDamageForce(self:GetBlastForce())
		info:SetDamagePosition(self:GetPos())
		info:SetDamage(exp_magnitude)
		info:SetDamageType(DMG_BLAST)
		info:SetReportedPosition(vecReported)

		swcs.RadiusDamage(info, self:GetPos(), exp_radius, false)
	end

	if SERVER or (CLIENT and IsFirstTimePredicted()) then
		local contents = util.PointContents(self:GetPos())
		local surfacedata = util.GetSurfaceData(tr.SurfaceProps)

		local vecParticleOrigin = self:GetPos()

		local effectName = self:GetParticleSystemName(contents, surfacedata)
		if bit.band(contents, MASK_WATER) ~= 0 then
			-- Find our water surface by tracing up till we're out of the water
			local tr2 = util.TraceLine({
				start = vecParticleOrigin,
				endpos = vecParticleOrigin + Vector(0,0, MAX_WATER_SURFACE_DISTANCE),
				mask = MASK_WATER,
			})

			-- if we didn't start in water, we're above it
			if not tr2.StartSolid then
				-- look downward to find the surface
				util.TraceLine({
					start = vecParticleOrigin,
					endpos = vecParticleOrigin - Vector(0,0, MAX_WATER_SURFACE_DISTANCE),
					mask = MASK_WATER,
					output = tr2
				})

				-- if we hit it, setup the explosion
				if tr2.Fraction < 1 then
					vecParticleOrigin:Set(tr2.HitPos)
				end
			elseif tr2.FractionLeftSolid > 0 then
				-- otherwise we came out of the water at this point
				vecParticleOrigin:Add(Vector(0,0, MAX_WATER_SURFACE_DISTANCE) * tr2.FractionLeftSolid)
			end
		end

		ParticleEffect(effectName, vecParticleOrigin, Angle())
	end

	self:SetSolid(SOLID_NONE)
	self:AddEffects(EF_NODRAW)
	--self:SetFinalVelocity(vector_origin)
	self:SetVelocity(vector_origin)
end

function ENT:GetParticleSystemName(pointcontents, surfData)
	if bit.band(pointcontents, MASK_WATER) ~= 0 then
		return "explosion_basic_water"
	end

	if surfData then
		local mat = surfData.material

		if mat == MAT_DIRT or
			mat == MAT_SAND or
			mat == MAT_GRASS or
			--mat == MAT_MUD or
			mat == MAT_FOLIAGE
			then
			return "explosion_hegrenade_dirt"
		elseif mat == MAT_SNOW then
			return "explosion_hegrenade_snow"
		end
	end

	return "explosion_basic"
end
